package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.dto.ISessionDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.configuration.ServerConfiguration;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.t1.yd.tm.constant.SessionTestData.*;
import static com.t1.yd.tm.constant.UserTestData.*;


@Tag("com.t1.yd.tm.marker.UnitCategory")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class SessionDTOServiceTest {

    @Autowired
    private ISessionDtoService service;

    @Autowired
    private IUserDtoService userDtoService;

    @BeforeEach
    public void initRepository() {
        service.clear();
        userDtoService.clear();

        userDtoService.add(ALL_USER_DTOS);
    }

    @Test
    public void add() {
        service.add(SESSION_DTO_1);
        Assertions.assertEquals(SESSION_DTO_1.getId(), service.findOneById(SESSION_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        Assertions.assertEquals(SESSION_DTO_1.getId(), service.findAll(USER_DTO_1.getId()).get(0).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_SESSION_DTOS);
        Assertions.assertEquals(ALL_SESSION_DTOS.size(), service.findAll().size());
    }

    @Test
    public void clear() {
        service.add(ALL_SESSION_DTOS);
        service.clear();
        Assertions.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        service.add(ADMIN.getId(), SESSION_DTO_2);
        Assertions.assertEquals(1, service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void existsById() {
        service.add(SESSION_DTO_1);
        Assertions.assertTrue(service.existsById(SESSION_DTO_1.getId()));
        Assertions.assertFalse(service.existsById(SESSION_DTO_2.getId()));
    }

    @Test
    public void removeById() {
        service.add(ALL_SESSION_DTOS);
        service.removeById(SESSION_DTO_1.getId());
        Assertions.assertEquals(ALL_SESSION_DTOS.size() - 1, service.findAll().size());
        Assertions.assertNull(service.findOneById(SESSION_DTO_1.getId()));
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        service.add(ADMIN.getId(), SESSION_DTO_2);

        Assertions.assertEquals(SESSION_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), SESSION_DTO_1.getId()).getId());
        Assertions.assertNull(service.findOneById(ADMIN.getId(), SESSION_DTO_1.getId()));
    }

    @AfterEach
    public void clearData() {
        service.clear();
        userDtoService.clear();
    }

}
