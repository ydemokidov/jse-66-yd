package com.t1.yd.tm.constant;

import com.t1.yd.tm.dto.model.SessionDTO;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER_DTO_1;

@UtilityClass
public class SessionTestData {

    @NotNull
    public final static String SESSION_NAME = "SESSION_NAME";

    @NotNull
    public final static String SESSION_DESCRIPTION = "SESSION_DESCRIPTION";

    @NotNull
    public final static SessionDTO SESSION_DTO_1 = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION_DTO_2 = new SessionDTO();

    @NotNull
    public final static List<SessionDTO> ALL_SESSION_DTOS = Arrays.asList(SESSION_DTO_1, SESSION_DTO_2);

    static {
        SESSION_DTO_1.setUserId(USER_DTO_1.getId());
        SESSION_DTO_1.setDate(new Date());
        SESSION_DTO_1.setRole(USER_DTO_1.getRole());

        SESSION_DTO_2.setUserId(ADMIN.getId());
        SESSION_DTO_2.setDate(new Date());
        SESSION_DTO_2.setRole(ADMIN.getRole());
    }

}

