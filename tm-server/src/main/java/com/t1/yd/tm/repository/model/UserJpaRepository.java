package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserJpaRepository extends AbstractJpaRepository<User> {

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

}
