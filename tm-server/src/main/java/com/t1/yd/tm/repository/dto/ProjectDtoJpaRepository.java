package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.dto.model.ProjectDTO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
public interface ProjectDtoJpaRepository extends AbstractDtoUserOwnedJpaRepository<ProjectDTO> {


}
