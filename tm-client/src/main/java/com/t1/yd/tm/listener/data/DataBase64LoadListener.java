package com.t1.yd.tm.listener.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataBase64LoadRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "load_base64";
    @NotNull
    private final String description = "Load data from base64 file";

    @Autowired
    public DataBase64LoadListener(@NotNull final ITokenService tokenService,
                                  @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64LoadListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[BASE64 DATA LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest();
        request.setToken(getToken());
        getDataEndpointClient().base64Load(request);
        System.out.println("[DATA LOADED]");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }
}
