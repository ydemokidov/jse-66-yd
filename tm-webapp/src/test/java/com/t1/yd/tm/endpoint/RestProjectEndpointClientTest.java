package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.client.RestProjectEndpointClient;
import com.t1.yd.tm.client.RestTaskEndpointClient;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static com.t1.yd.tm.constant.ProjectTestData.PROJECT1_DESCRIPTION;
import static com.t1.yd.tm.constant.ProjectTestData.PROJECT1_NAME;
import static com.t1.yd.tm.constant.TaskTestData.*;
import static com.t1.yd.tm.constant.UserTestData.USER1;

@Category(IntegrationCategory.class)
public class RestProjectEndpointClientTest {


    @NotNull
    private final RestTaskEndpointClient taskEndpointClient = RestTaskEndpointClient.client();

    @NotNull
    private final RestProjectEndpointClient projectEndpointClient = RestProjectEndpointClient.client();

    @NotNull
    private ProjectDTO project1 = new ProjectDTO(USER1.getId(), PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    private TaskDTO task1 = new TaskDTO(USER1.getId(), TASK1_NAME, TASK1_DESCRIPTION);

    @NotNull
    private TaskDTO task2 = new TaskDTO(USER1.getId(), TASK2_NAME, TASK2_DESCRIPTION);

    @NotNull
    private TaskDTO task3 = new TaskDTO(USER1.getId(), TASK3_NAME, TASK3_DESCRIPTION);

    private long count = 0;

    @Before
    public void before() throws Exception {
        count = taskEndpointClient.findAll().size();
        projectEndpointClient.add(project1);
        task1.setProjectId(project1.getId());
        taskEndpointClient.add(task1);
        taskEndpointClient.add(task2);
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<TaskDTO> tasks = taskEndpointClient.findAll();
        Assert.assertEquals(count + 2, tasks.size());
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        @NotNull final List<TaskDTO> tasks = taskEndpointClient.findByProjectId(task1.getProjectId());
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(task1.getName(), tasks.get(0).getName());
        Assert.assertEquals(task1.getDescription(), tasks.get(0).getDescription());
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull TaskDTO task = taskEndpointClient.add(task3);
        Assert.assertEquals(task3.getName(), task.getName());
        Assert.assertEquals(task3.getDescription(), task.getDescription());
    }

    @Test
    public void testUpdate() throws Exception {
        @NotNull TaskDTO task = taskEndpointClient.findById(task1.getId());
        task.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(taskEndpointClient.update(task));
        @Nullable TaskDTO foundTask = taskEndpointClient.findById(task1.getId());
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task.getStatus(), task2.getStatus());
    }

    @Test
    public void testFindById() throws Exception {
        @Nullable TaskDTO task = taskEndpointClient.findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK1_NAME, task.getName());
        Assert.assertEquals(TASK1_DESCRIPTION, task.getDescription());
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertTrue(taskEndpointClient.existsById(task1.getId()));
        Assert.assertFalse(taskEndpointClient.existsById(task3.getId()));
    }

    @Test
    public void testCount() throws Exception {
        Assert.assertEquals(count + 2, taskEndpointClient.count());
    }

    @Test
    public void testDeleteById() throws Exception {
        taskEndpointClient.deleteById(task1.getId());
        Assert.assertNull(taskEndpointClient.findById(task1.getId()));
    }

    @Test
    public void testDelete() throws Exception {
        taskEndpointClient.delete(task1);
        Assert.assertNull(taskEndpointClient.findById(task1.getId()));
    }

    @After
    public void after() throws Exception {
        taskEndpointClient.delete(task1);
        taskEndpointClient.delete(task2);
        taskEndpointClient.delete(task3);
        projectEndpointClient.delete(project1);
    }

}
