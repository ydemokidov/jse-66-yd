package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IPropertyService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("${database.server}")
    private String dbServer;

    @Value("${server.host}")
    private String host;

    @Value("${server.port}")
    private Integer serverPort;

    @Value("${database.username}")
    private String dbUsername;

    @Value("${database.password}")
    private String dbPassword;

    @Value("${database.driver}")
    private String dbDriver;

    @Value("${database.dialect}")
    private String dbSqlDialect;

    @Value("${database.showSql}")
    private String dbShowSqlFlg;

    @Value("${database.strategy}")
    private String dbStrategy;

    @Value("${database.cache.use_second_level_cache}")
    private String dbCacheUseSecondLevel;

    @Value("${database.cache.use_query_cache}")
    private String dbCacheUseQuery;

    @Value("${database.cache.use_minimal_puts}")
    private String dbCacheUseMinPuts;

    @Value("${database.cache.region_prefix}")
    private String dbCacheRegionPrefix;

    @Value("${database.cache.provider_config_file_resource_path}")
    private String dbCacheProviderConfig;

    @Value("${database.cache.factory_class}")
    private String dbCacheFactoryClass;

}
