package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.DescriptionEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.NameEmptyException;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final TaskRepository repository;

    @Autowired
    public TaskService(@NotNull final IProjectService projectService,
                       @NotNull final TaskRepository repository) {
        this.projectService = projectService;
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public Task add(@NotNull final Task task) throws Exception {
        return repository.save(task);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<Task> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable final Task model) throws Exception {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void update(@Nullable final Task task) throws Exception {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String name, @Nullable final String description) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByProject(projectService.findOneById(projectId));
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
    }

    @Override
    @Transactional
    public void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectService.findOneById(projectId));
        repository.save(task);
    }


}
