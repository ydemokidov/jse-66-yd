package com.t1.yd.tm.client;

import com.t1.yd.tm.dto.model.TaskDTO;
import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface RestTaskEndpointClient {

    @NotNull String url = "http://localhost:8080/api/task";

    static RestTaskEndpointClient client() {
        @NotNull final FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(formHttpMessageConverter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(RestTaskEndpointClient.class, url);
    }

    @GetMapping("/list")
    List<TaskDTO> findAll() throws Exception;

    @PostMapping("/add")
    TaskDTO add(@RequestBody TaskDTO taskDto) throws Exception;

    @PostMapping("/update")
    TaskDTO update(@RequestBody TaskDTO taskDto) throws Exception;

    @GetMapping("/{id}")
    TaskDTO findById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/exists/{id}")
    boolean existsById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/byProject/{projectId}")
    List<TaskDTO> findByProjectId(@PathVariable("projectId") String projectId) throws Exception;

    @GetMapping("/count")
    long count() throws Exception;

    @PostMapping("/delete/{id}")
    void deleteById(@PathVariable("id") String id) throws Exception;

    @PostMapping("/delete")
    void delete(@RequestBody TaskDTO taskDto) throws Exception;

}
