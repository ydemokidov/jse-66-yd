package com.t1.yd.tm.dto.response.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class UserLoginResponse extends AbstractUserResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse() {
        super();
    }

}
